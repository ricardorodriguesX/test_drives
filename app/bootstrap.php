<?php

$request = $_SERVER['REQUEST_URI'];

$request = str_replace("-", "_", $request);

if (strpos($request, '/api') !== false) {
   apiRouter($request);

   return;
}

viewRouter($request);

function apiRouter($request)
{
   $endpoint = strtok($request, '?');
   
   $endpoint = str_replace("/api/", "", $endpoint);

   $endpoint = trim($endpoint, '/');
   
   $endpoint = __DIR__ . '/server/endpoints/' . $endpoint . ".php";

   if (!file_exists($endpoint)) {
      http_response_code(404);

      require_once __DIR__ . '/public/errors/404.php';

      return;
   }

   require_once $endpoint;
}

function viewRouter($request)
{
   $request = trim($request, '/');

   $view = __DIR__ . "/public/views/$request.php";

   if (file_exists($view)) {
      require_once $view;

      return;
   }

   $view = __DIR__ . "/public/views/$request/index.php";

   if (!file_exists($view)) {
      http_response_code(404);
      require_once __DIR__ . '/public/errors/404.php';
      return;
   }

   require_once $view;
}
