<footer class="footer">
   <div class="container">
      <div class="row">
         <div class="col-12 col-sm-4">
            <p>© <?= date('Y') ?> Overdrive</p>
         </div>
         <div class="col-12 col-sm-4">
            <ul>
               <li>
                  <a class="link-light" href="/">Página Principal</a>
               </li>
               <li>
                  <a class="link-light" href="cars">Carros</a>
               </li>
               <li>
                  <a class="link-light" href="marcacoes">Marcações</a>
               </li>
               <li>
                  <a class="link-light" href="quem-somos">Quem Somos</a>
               </li>
               <li>
                  <a class="link-light" href="onde-estamos">Onde Estamos</a>
               </li>
               <li>
                  <a class="link-light" href="politica-privacidade">Política de Privacidade</a>
               </li>
            </ul>
         </div>
         <div class="col-12 col-sm-4">
            <ul class="list-inline">
               <li class="list-inline-item">
                  <a class="link-light lead" href="https://www.facebook.com" target="_blank"><i class="bi bi-facebook"></i></a>
               </li>
               <li class="list-inline-item">
                  <a class="link-light lead" href="https://www.twitter.com" target="_blank"><i class="bi bi-twitter"></i></a>
               </li>
               <li class="list-inline-item">
                  <a class="link-light lead" href="https://www.linkedin.com" target="_blank"><i class="bi bi-linkedin"></i></a>
               </li>
            </ul>
         </div>
      </div>
   </div>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script src="/app/public/assets/scripts/entities/Customer.js"></script>
<script src="/app/public/assets/scripts/entities/Type.js"></script>
<script src="/app/public/assets/scripts/entities/Brand.js"></script>
<script src="/app/public/assets/scripts/entities/Car.js"></script>
<script src="/app/public/assets/scripts/entities/TestDrive.js"></script>