<header>
   <div class="container-fluid">
      <div class="row">
         <div class="col-10 offset-1">
            <nav class="navbar navbar-expand-lg">
               <div class="container-fluid">
                  <a class="navbar-brand" href="/"><img class="img img-fluid" src="/app/public/assets/images/logo.png" alt="Overdrive Logo"></a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#responsiveNav" aria-controls="responsiveNav" aria-expanded="false">
                     <i class="bi bi-list"></i>
                  </button>
                  <div class="collapse navbar-collapse justify-content-end" id="responsiveNav">
                     <ul class="navbar-nav">
                        <li class="nav-item">
                           <a class="nav-link active" href="/">Página Principal</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="cars">Carros</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="marcacoes">Marcações</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="quem-somos">Quem Somos</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="onde-estamos">Onde Estamos</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </nav>
         </div>
      </div>
   </div>
</header>