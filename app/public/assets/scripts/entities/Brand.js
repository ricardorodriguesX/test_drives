class Brand {
    static search(params) {
        return $.ajax({
            type: "GET",
            url: "/api/brands/search",
            dataType: "json",
            data: params
        })
    }
}