class Car {
    static search(params) {
        return $.ajax({
            type: "GET",
            url: "/api/cars/search",
            dataType: "json",
            data: params
        })
    }

    static get(id) {
        return $.ajax({
            type: "GET",
            url: "/api/cars/get",
            dataType: "json",
            data: {
                id: id
            }
        })
    }
}