class Customer {

    static search(params) {
        return $.ajax({
            type: "GET",
            url: "/api/customers/search",
            dataType: "json",
            data: params
        })
    }

    static get(email) {
        return $.ajax({
            type: "GET",
            url: "/api/customers/get",
            dataType: "json",
            data: {
                email: email
            }
        })
    }

    static create(params) {
        return $.ajax({
            type: "POST",
            url: "/api/customers/create",
            dataType: "json",
            data: params
        })
    }
}