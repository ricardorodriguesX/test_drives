class TestDrive {
    static search(params) {
        return $.ajax({
            type: "GET",
            url: "/api/test-drives/search",
            dataType: "json",
            data: params
        })
    }

    static get(id) {
        return $.ajax({
            type: "GET",
            url: "/api/test-drives/get",
            dataType: "json",
            data: {
                id: id
            }
        })
    }

    static create(params) {
        return $.ajax({
            type: "POST",
            url: "/api/test-drives/create",
            dataType: "json",
            data: params
        })
    }
}