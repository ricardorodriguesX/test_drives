class Type {
    static search(params) {
        return $.ajax({
            type: "GET",
            url: "/api/types/search",
            dataType: "json",
            data: params
        })
    }
}