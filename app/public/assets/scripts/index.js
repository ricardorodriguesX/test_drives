let step = 1;

$(document).ready(function () {
    fillCars();

    fillBrands();

    submitTestDrive();
});

function fillCars()
{
    Car.search()
        .done(function (response) {

            response = response.slice(0, 4);


            for (car of response) {
                
                let html = `
                    <div class="col-sm-12 col-md-4 col-lg-3 mb-5 car" data-car='`+car.id+`' data-bs-toggle="modal" data-bs-target="#testDriveModal">
                        <div class="card mb-3">
                            <img src="`+car.image+`" class="card-img-top car-img" alt="`+car.model+ '-' + car.registration + `">
                            <div class="card-body">
                                <h5 class="card-title">`+car.brand_name+ " " +car.model+ " (" + car.type_name +`)</h5>
                                <p class="card-text"><small class="text-muted">`+car.registration+`</small></p>
                            </div>
                        </div>
                    </div>
                `;

                $("#cars").append(html);
            }

            testDrive();
        })
        .fail();
}

function fillBrands()
{
    Brand.search()
        .done(function (response) {
            for (brand of response) {
                
                let html = `
                    <div class="col-sm-12 col-md-4 text-center mb-5">
                           <img src="`+brand.image+`" class="img img-fluid" alt="`+brand.name+`">
                        </div>
                `;

                $("#brands").append(html);
            }
        })
        .fail();
}

function testDrive()
{
    $(".car").click(function (event) {
        step = 1;

        $("#car_id").val($(this).data("car"));

        disableBookedDates();

        $("#customer_id").val("");

        $(".step-2, .step-3, #success").addClass("visually-hidden");
            
        $(".step-1, .modal-footer, #testDrive").removeClass("visually-hidden");
    })
}

function disableBookedDates()
{
    TestDrive.search({car_id: $("#car_id").val()})
        .done(function (response) {

            let dates = response.map(function (testDrive) {

                let date = testDrive.date.split(" ");

                return date[0];
            });

            $('#date').datepicker({
                dateFormat: 'yy-mm-dd',
                startDate: "+1",
                beforeShowDay: function(date) {
                    var string = $.datepicker.formatDate('yy-mm-dd', date);

                    return [dates.indexOf(string) == -1];
                },
                autoclose: true,
                todayHighlight: true
            });
        });
}

function submitTestDrive()
{
    $("#next").click(function (event) {
        switch (step) {
            case 1:
                $("#testDrive").validate().element("#email");

                if ($("#email").valid()) {
                    findCustomer();
                }

                break;
            case 2:
                $("#testDrive").validate().element("#name");

                $("#testDrive").validate().element("#phone");

                $("#testDrive").validate().element("#locality");

                if ($("#name").valid() && $("#phone").valid() && $("#locality").valid()) {
                    submitCustomer();
                }

                break;
            case 3:
                $("#testDrive").validate().element("#date");

                if ($("#date").valid()) {
                    createTestDrive();
                }

                break;

        }
    });
}

function findCustomer()
{
    Customer.get($("#email").val())
        .done(function (response) {
            step = 3;

            $("#customer_id").val(response.id);

            $(".step-1, .step-2").addClass("visually-hidden");

            $(".step-3").removeClass("visually-hidden");
        }).fail(function (response) {

            step = 2;

            $(".step-1, .step-3").addClass("visually-hidden");
            
            $(".step-2").removeClass("visually-hidden");
        });
}

function submitCustomer()
{
    Customer.create({
        email: $("#email").val(),
        name: $("#name").val(),
        phone: $("#name").val(),
        locality: $("#locality").val(),
    })
        .done(function (response) {
            step = 3;

            $("#customer_id").val(response.id);

            $(".step-1,.step-2").addClass("visually-hidden");
            
            $(".step-3").removeClass("visually-hidden");
        });
}

function createTestDrive()
{
    TestDrive.create({
        date: $("#date").val(),
        customer_id: $("#customer_id").val(),
        car_id: $("#car_id").val(),
    }).done(function (response) {
        $("#success").removeClass("visually-hidden");

        $("#testDrive, .modal-footer").addClass("visually-hidden");

        $('#date').val("");

        $('#date').datepicker('destroy')
    });
}