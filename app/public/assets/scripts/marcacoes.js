let testDrivesFilters = {};

$(document).ready(function () {
    fillTestDrives();

    fillCars();

    fillCustomers();

    search();
});

function fillTestDrives()
{
    $("#testDrives").children().remove();

    TestDrive.search(testDrivesFilters)
        .done(function (response) {

            for (testDrive of response) {
                
                let html = `
                   <tr>
                        <th scope="row">` + testDrive.id + `</th>
                        <td>` + testDrive.customer_name + " (" +testDrive.customer_email + `)</td>
                        <td>` + testDrive.car_registration + " - " + testDrive.car_brand + " " + testDrive.car_model + " (" + testDrive.car_type +  `)</td>
                        <td>` + testDrive.date + `</td>
                    </tr>
                `;

                $("#testDrives").append(html);
            }
        })
        .fail();
}

function fillCars()
{
    Car.search()
        .done(function (response) {
            console.log(response);
            for (car of response) {
                
                let html = `
                    <option value='` + car.id + `'>
                           `+car.brand_name+ " " + car.model +`
                    </option>
                `;

                $("#car_id").append(html);
            }
        })
        .fail();
}

function fillCustomers()
{
    Customer.search()
        .done(function (response) {
            for (customer of response) {
                
                let html = `
                    <option value='` + customer.id + `'>
                           `+ customer.name + " (" +customer.email + `)`+`
                    </option>
                `;

                $("#customer_id").append(html);
            }
        })
        .fail();
}

function search()
{
    $("#searchTestDrives").submit(function (event) {
       event.preventDefault();
       
        testDrivesFilters.customer_id = $("#customer_id").val();

        testDrivesFilters.car_id = $("#car_id").val();

        fillTestDrives();
    });

    $("#resetSearch").click(function (event) {

        testDrivesFilters = {};

        setTimeout(function () {fillTestDrives();}, 500);
    })
}