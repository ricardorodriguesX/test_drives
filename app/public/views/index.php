<!DOCTYPE html>
<html lang="pt">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Home - <?= parse_ini_file('.env')["APP_NAME"] ?></title>
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
   <link href="https://code.jquery.com/ui/1.12.1/themes/black-tie/jquery-ui.css" rel="stylesheet">
   <link href="/app/public/assets/styles/style.css" rel="stylesheet">
</head>

<body>
   <div class="wrapper">
      <?php include_once __DIR__ . '/../assets/components/header.php' ?>
      <main>
         <section class="hero" style="background-image: url('/app/public/assets/images/home_hero.jpg')">
            <div class="v-center">
               <h1 class="display-1 highlight">Encontre o seu verdadeiro carro</h1>
            </div>
         </section>
         <section class="mt-5">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-10 offset-1">
                     <h2 class="display-2">Os nossos Carros</h2>

                     <div class="row" id="cars">
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="mt-5">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-10 offset-1">
                     <h2 class="display-2">As nossas Marcas</h2>

                     <div class="row" id="brands">
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </main>
      <?php include_once __DIR__ . '/../assets/components/footer.php' ?>
      <script src="/app/public/assets/scripts/index.js"></script>
   </div>
   <div class="modal fade" id="testDriveModal" tabindex="-1" aria-labelledby="testDriveModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title display-5" id="testDriveModalLabel">Marcação de Test Drive</h5>
               <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
               <form id="testDrive">
                  <input class="visually-hidden" name="car_id" id="car_id" />

                  <input class="visually-hidden" name="customer_id" id="customer_id" />

                  <div class="row step-1">
                     <div class="col-12 mb-3">
                        <input class="form-control" type="email" name="email" id="email" required placeholder="Indique o seu email">
                     </div>
                  </div>

                  <div class="row step-2 visually-hidden">
                     <div class="col-12 mb-3">
                        <input class="form-control" type="text" name="name" id="name" required placeholder="Indique o seu nome">
                     </div>

                     <div class="col-md-12 col-lg-6 mb-3">
                        <input class="form-control" type="text" name="phone" id="phone" required placeholder="Indique o seu telefone">
                     </div>

                     <div class="col-md-12 col-lg-6 mb-3">
                        <input class="form-control" type="text" name="locality" id="locality" required placeholder="Indique a sua localidade">
                     </div>
                  </div>

                  <div class="row step-3 visually-hidden">
                     <div class="col-12 mb-3">
                        <input class="form-control" type="text" name="date" id="date" required placeholder="Indique a data pretendida">
                     </div>
                  </div>
               </form>
               <div class="text-center visually-hidden" id="success">
                  <p class="display-6 text-success"><i class="bi bi-check-circle"></i></p>
                  <p class="display-6 text-success">Test Drive registado!</p>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-dark" id="next">Próximo</button>
            </div>
         </div>
      </div>
   </div>
</body>

</html>