<!DOCTYPE html>
<html lang="pt">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Marcações - <?= parse_ini_file('.env')["APP_NAME"] ?></title>
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
   <link href="https://code.jquery.com/ui/1.12.1/themes/black-tie/jquery-ui.css" rel="stylesheet">
   <link href="/app/public/assets/styles/style.css" rel="stylesheet">
</head>

<body>
   <div class="wrapper">
      <?php include_once __DIR__ . '/../../assets/components/header.php' ?>
      <main>
         <section class="hero" style="background-image: url('/app/public/assets/images/cars_hero.jpg')">
            <div class="v-center">
               <h1 class="display-1 highlight">Marcações</h1>
            </div>
         </section>
         <section class="mt-5">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-10 offset-1">
                     <h2 class="display-2">Pesquisar</h2>

                     <form id="searchTestDrives" class="col-md-12 col-lg-6 mt-4">

                        <div class="row">
                           <div class="col-sm-12 col-md-4 mb-3">
                              <select class="form-select" id="car_id" name="car_id">
                                 <option value="" hidden selected>Selecione o carro</option>
                              </select>
                           </div>

                           <div class="col-sm-12 col-md-4 mb-3">
                              <select class="form-select" id="customer_id" name="customer_id">
                                 <option value="" hidden selected>Selecione o cliente</option>
                              </select>
                           </div>
                           <div class="col-sm-12 col-md-4 mb-3">
                              <button class="btn btn-dark mb-3">Pesquisar</button>
                              <button class="btn btn-dark mb-3" id="resetSearch" type="reset">Limpar</button>
                           </div>
                        </div>
                     </form>

                     <table class="table table-dark table-striped table-hover mt-5">
                        <thead>
                           <tr>
                              <th scope="col">#</th>
                              <th scope="col">Cliente</th>
                              <th scope="col">Carro</th>
                              <th scope="col">Data</th>
                           </tr>
                        </thead>
                        <tbody id="testDrives">
                           <tr>
                              <th scope="row">1</th>
                              <td>Mark</td>
                              <td>Otto</td>
                              <td>@mdo</td>
                           </tr>
                           <tr>
                              <th scope="row">2</th>
                              <td>Jacob</td>
                              <td>Thornton</td>
                              <td>@fat</td>
                           </tr>
                           <tr>
                              <th scope="row">3</th>
                              <td colspan="2">Larry the Bird</td>
                              <td>@twitter</td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </section>
      </main>
      <?php include_once __DIR__ . '/../../assets/components/footer.php' ?>
      <script src="/app/public/assets/scripts/marcacoes.js"></script>
   </div>
</body>

</html>