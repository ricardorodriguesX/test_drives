<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Onde Estamos - <?= parse_ini_file('.env')["APP_NAME"] ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="https://code.jquery.com/ui/1.12.1/themes/black-tie/jquery-ui.css" rel="stylesheet">
    <link href="/app/public/assets/styles/style.css" rel="stylesheet">
</head>

<body>
<div class="wrapper">
    <?php include_once __DIR__ . '/../assets/components/header.php' ?>
    <main>
        <section class="hero" style="background-image: url('/app/public/assets/images/onde_estamos_hero.jpg')">
            <div class="v-center">
                <h1 class="display-1 highlight">Onde Estamos</h1>
            </div>
        </section>
        <section class="mt-5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-10 offset-1">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h2 class="display-2">Onde Estamos</h2>

                                <p>
                                    <strong>Morada: </strong>
                                    Edifício <?= parse_ini_file('.env')["APP_NAME"] ?>, N262 Km 66,7630-174 Odemira , Portugal
                                </p>

                                <p>
                                    <strong>Coordenadas GPS:</strong> 37.811926, -8.505124
                                </p>

                                <p>
                                    <strong> Horário de funcionamento:</strong>
                                </p>
                                <ul>
                                    <li>Segunda á Sexta-Feira das 09:00 ás 19:30</li>
                                    <li>Sabádo das 09:00 ás 18:00</li>
                                    <li>Domingos e Feridos só com marcação prévia</li>
                                </ul>

                                <p>
                                    <strong>Contactos:</strong>
                                </p>
                                <ul>
                                    <li>Telf. 123 456 789</li>
                                    <li>Telm. 913 456 789</li>
                                    <li>Email: geral@<?= strtolower(parse_ini_file('.env')["APP_NAME"]) ?>.pt</li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <img src="/app/public/assets/images/map.png" class="img img-fluid" alt="Mapa Onde estamos">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
    <?php include_once __DIR__ . '/../assets/components/footer.php' ?>
</div>
</body>

</html>