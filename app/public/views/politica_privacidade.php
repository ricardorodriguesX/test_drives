<!DOCTYPE html>
<html lang="pt">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Política de Privacidade - <?= parse_ini_file('.env')["APP_NAME"] ?></title>
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
   <link href="https://code.jquery.com/ui/1.12.1/themes/black-tie/jquery-ui.css" rel="stylesheet">
   <link href="/app/public/assets/styles/style.css" rel="stylesheet">
</head>

<body>
   <div class="wrapper">
      <?php include_once __DIR__ . '/../assets/components/header.php' ?>
      <main>
         <section class="hero" style="background-image: url('/app/public/assets/images/privacy_policy_hero.jpg')">
            <div class="v-center">
               <h1 class="display-1 highlight">Política de Privacidade</h1>
            </div>
         </section>
         <section class="mt-5">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-10 offset-1">
                     <h2 class="display-2">Lorem ipsum</h2>

                     <p>
                        Fugiat deserunt culpa laboris dolore dolor sint commodo id id minim magna deserunt non. Id est qui duis ipsum magna eu. Ex cillum adipisicing enim mollit. Dolore dolore nulla quis amet.
                     </p>

                     <p>
                        Non magna est duis voluptate nisi laboris cupidatat sint reprehenderit ea. Nulla nulla nisi esse culpa exercitation occaecat magna sunt consequat minim consectetur aute eiusmod. Elit officia est ad do. Eiusmod dolore sit quis ea reprehenderit. Ad Lorem dolore reprehenderit occaecat ex nostrud aliquip minim in exercitation duis ea. Nulla anim nostrud minim ad deserunt consectetur nulla et labore in cupidatat aute. Non do ex fugiat velit incididunt.
                     </p>

                     <p>
                        Esse in eu dolor tempor tempor Lorem irure consectetur ad. Aute proident ex nisi Lorem elit elit eiusmod nostrud esse ex. Ipsum non exercitation voluptate est ullamco aute officia ipsum. Deserunt tempor adipisicing amet adipisicing et sint quis magna labore aute fugiat irure commodo. Dolor tempor labore aute incididunt consectetur voluptate elit dolore. Deserunt qui quis fugiat voluptate. Dolor eiusmod deserunt officia aliqua irure nulla pariatur reprehenderit.
                     </p>

                     <p>
                        Velit aliqua consectetur labore proident pariatur incididunt est veniam. Consequat dolor velit laborum qui fugiat aliqua duis quis fugiat occaecat in ullamco. Tempor excepteur aute consequat dolore proident. Deserunt excepteur elit mollit mollit cillum eiusmod.
                     </p>
                  </div>

                  <div class="col-10 offset-1">
                     <h2>Lorem ipsum</h2>

                     <p>
                        Fugiat deserunt culpa laboris dolore dolor sint commodo id id minim magna deserunt non. Id est qui duis ipsum magna eu. Ex cillum adipisicing enim mollit. Dolore dolore nulla quis amet.
                     </p>

                     <p>
                        Non magna est duis voluptate nisi laboris cupidatat sint reprehenderit ea. Nulla nulla nisi esse culpa exercitation occaecat magna sunt consequat minim consectetur aute eiusmod. Elit officia est ad do. Eiusmod dolore sit quis ea reprehenderit. Ad Lorem dolore reprehenderit occaecat ex nostrud aliquip minim in exercitation duis ea. Nulla anim nostrud minim ad deserunt consectetur nulla et labore in cupidatat aute. Non do ex fugiat velit incididunt.
                     </p>

                     <p>
                        Esse in eu dolor tempor tempor Lorem irure consectetur ad. Aute proident ex nisi Lorem elit elit eiusmod nostrud esse ex. Ipsum non exercitation voluptate est ullamco aute officia ipsum. Deserunt tempor adipisicing amet adipisicing et sint quis magna labore aute fugiat irure commodo. Dolor tempor labore aute incididunt consectetur voluptate elit dolore. Deserunt qui quis fugiat voluptate. Dolor eiusmod deserunt officia aliqua irure nulla pariatur reprehenderit.
                     </p>

                     <p>
                        Velit aliqua consectetur labore proident pariatur incididunt est veniam. Consequat dolor velit laborum qui fugiat aliqua duis quis fugiat occaecat in ullamco. Tempor excepteur aute consequat dolore proident. Deserunt excepteur elit mollit mollit cillum eiusmod.
                     </p>
                  </div>

                  <div class="col-10 offset-1">
                     <h2>Lorem ipsum</h2>

                     <p>
                        Fugiat deserunt culpa laboris dolore dolor sint commodo id id minim magna deserunt non. Id est qui duis ipsum magna eu. Ex cillum adipisicing enim mollit. Dolore dolore nulla quis amet.
                     </p>

                     <p>
                        Non magna est duis voluptate nisi laboris cupidatat sint reprehenderit ea. Nulla nulla nisi esse culpa exercitation occaecat magna sunt consequat minim consectetur aute eiusmod. Elit officia est ad do. Eiusmod dolore sit quis ea reprehenderit. Ad Lorem dolore reprehenderit occaecat ex nostrud aliquip minim in exercitation duis ea. Nulla anim nostrud minim ad deserunt consectetur nulla et labore in cupidatat aute. Non do ex fugiat velit incididunt.
                     </p>

                     <p>
                        Esse in eu dolor tempor tempor Lorem irure consectetur ad. Aute proident ex nisi Lorem elit elit eiusmod nostrud esse ex. Ipsum non exercitation voluptate est ullamco aute officia ipsum. Deserunt tempor adipisicing amet adipisicing et sint quis magna labore aute fugiat irure commodo. Dolor tempor labore aute incididunt consectetur voluptate elit dolore. Deserunt qui quis fugiat voluptate. Dolor eiusmod deserunt officia aliqua irure nulla pariatur reprehenderit.
                     </p>

                     <p>
                        Velit aliqua consectetur labore proident pariatur incididunt est veniam. Consequat dolor velit laborum qui fugiat aliqua duis quis fugiat occaecat in ullamco. Tempor excepteur aute consequat dolore proident. Deserunt excepteur elit mollit mollit cillum eiusmod.
                     </p>
                  </div>

                  <div class="col-10 offset-1">
                     <h2>Lorem ipsum</h2>

                     <p>
                        Fugiat deserunt culpa laboris dolore dolor sint commodo id id minim magna deserunt non. Id est qui duis ipsum magna eu. Ex cillum adipisicing enim mollit. Dolore dolore nulla quis amet.
                     </p>

                     <p>
                        Non magna est duis voluptate nisi laboris cupidatat sint reprehenderit ea. Nulla nulla nisi esse culpa exercitation occaecat magna sunt consequat minim consectetur aute eiusmod. Elit officia est ad do. Eiusmod dolore sit quis ea reprehenderit. Ad Lorem dolore reprehenderit occaecat ex nostrud aliquip minim in exercitation duis ea. Nulla anim nostrud minim ad deserunt consectetur nulla et labore in cupidatat aute. Non do ex fugiat velit incididunt.
                     </p>

                     <p>
                        Esse in eu dolor tempor tempor Lorem irure consectetur ad. Aute proident ex nisi Lorem elit elit eiusmod nostrud esse ex. Ipsum non exercitation voluptate est ullamco aute officia ipsum. Deserunt tempor adipisicing amet adipisicing et sint quis magna labore aute fugiat irure commodo. Dolor tempor labore aute incididunt consectetur voluptate elit dolore. Deserunt qui quis fugiat voluptate. Dolor eiusmod deserunt officia aliqua irure nulla pariatur reprehenderit.
                     </p>

                     <p>
                        Velit aliqua consectetur labore proident pariatur incididunt est veniam. Consequat dolor velit laborum qui fugiat aliqua duis quis fugiat occaecat in ullamco. Tempor excepteur aute consequat dolore proident. Deserunt excepteur elit mollit mollit cillum eiusmod.
                     </p>
                  </div>
               </div>
            </div>
         </section>
      </main>
      <?php include_once __DIR__ . '/../assets/components/footer.php' ?>
   </div>
</body>

</html>