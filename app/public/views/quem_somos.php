<!DOCTYPE html>
<html lang="pt">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Quem Somos - <?= parse_ini_file('.env')["APP_NAME"] ?></title>
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
   <link href="https://code.jquery.com/ui/1.12.1/themes/black-tie/jquery-ui.css" rel="stylesheet">
   <link href="/app/public/assets/styles/style.css" rel="stylesheet">
</head>

<body>
   <div class="wrapper">
      <?php include_once __DIR__ . '/../assets/components/header.php' ?>
      <main>
         <section class="hero" style="background-image: url('/app/public/assets/images/quem_somos_hero.jpg')">
            <div class="v-center">
               <h1 class="display-1 highlight">Quem somos</h1>
            </div>
         </section>
         <section class="mt-5">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-10 offset-1 mb-4">
                     <h2 class="display-2">Quem somos</h2>

                     <p>
                        A <?= parse_ini_file('.env')["APP_NAME"] ?> quer construir relações de confiança com os seus clientes, respondendo às suas expectativas de qualidade, segurança e credibilidade no serviço prestado. Para isso, dispomos de uma grande variedade de veículos, testados e aprovados, que satisfazem todos os gostos da gama mais alta à gama mais económica. Para além deste tipo de veículos o stand também comercializa comerciais de qualidade e com garantia de venda. </p>

                     <p>
                        Complementando o serviço de venda automóvel, o espaço dispõe de uma parceria com a área de serviço pós-venda adjacente. Dedicado à manutenção e reparação de automóveis.
                     </p>

                  </div>

                  <div class="col-10 offset-1 mb-4">
                     <h2 class="display-2">Visão</h2>

                     <p>
                        Uma organização motivada e sustentável, virada para Clientes Felizes, com a Marca mais atrativa do mercado.
                     </p>

                  </div>

                  <div class="col-10 offset-1 mb-4">
                     <h2 class="display-2">Missão</h2>

                     <p>
                        Satisfazer permanentemente as necessidades e desejos de mobilidade de pessoas, através de produtos excecionais da marca Audi e de serviços de excelência dos seus profissionais.
                     </p>

                  </div>

                  <div class="col-10 offset-1 mb-4">
                     <h2 class="display-2">Valores</h2>

                     <p>
                        Os Valores da Expocar traduzem a nossa forma de ser e de estar:
                     </p>

                     <ul class="list-group list-group-flush mb-3 mt-3">
                        <li class="list-group-item">Responsabilidade</li>
                        <li class="list-group-item">Adaptabilidade</li>
                        <li class="list-group-item"> Cooperação</li>
                        <li class="list-group-item">Inovação</li>
                        <li class="list-group-item">Empreendedorismo</li>

                     </ul>
                     <p>
                        É com base na nossa Visão, Missão e Valores, que assumimos a ambição de conquistar a sua confiança para sermos o parceiro para a sua mobilidade e oferecer-lhe o nosso serviço após-venda que pretendemos ser de excelência. Queremos, numa palavra, estabelecer uma relação duradoura e de satisfação plena Consigo.
                     </p>
                  </div>
               </div>
            </div>
         </section>
      </main>
      <?php include_once __DIR__ . '/../assets/components/footer.php' ?>
   </div>
</body>

</html>