<?php

abstract class Action
{
   protected $attributes;

   abstract public function handle();

   public function __construct($attributes = [])
   {
      $this->attributes = $attributes;
   }

   public static function run($attributes = [])
   {
      $instance = new static($attributes);

      if (!$instance->authorize()) {
         header("HTTP/1.1 401 Unauthorized");
         exit;
      }

      $validations = [];

      $instance->rules($validations);

      if (!empty($validations)) {
         header("HTTP/1.1 412 Unauthorized");

         return json_encode($validations, true);
      }

      $return = $instance->handle();

      $return = empty($return) ? [] : $return;
      
      return json_encode($return, true);
   }

   public function __get($name)
   {
      return empty($this->attributes[$name]) ? null : $this->attributes[$name];
   }

   public function __set($name, $value)
   {
      return $this->attributes[$name] = $value;
   }

   public function authorize(): bool
   {
      return true;
   }

   public function rules(&$validations = [])
   {
      $validations;
   }

   public function abort($code)
   {
      http_response_code($code);

      exit;
   }

   public function abort_if($condition, $code)
   {
      if ($condition) {
         http_response_code($code);

         exit;
      }
   }
}
