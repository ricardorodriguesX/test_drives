<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/Brand.php';

class Search extends Action
{
   public function handle()
   {
      return Brand::all();
   }
}
