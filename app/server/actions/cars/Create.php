<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/Car.php';

class Create extends Action
{
   public function handle()
   {
      return Car::create([
         "registration"  => $this->registration,
         "model"         => $this->model,
         "brand_id"      => $this->brand_id,
         "type_id"       => $this->type_id,
         "image"         => $this->image,
      ])->toArray();
   }
}
