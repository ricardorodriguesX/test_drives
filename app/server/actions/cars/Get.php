<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/Car.php';

class Get extends Action
{
   public function handle()
   {
      $car = Car::find($this->id);

      $this->abort_if(empty($car), 404);

      return $car->toArray();
   }
}
