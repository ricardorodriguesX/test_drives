<?php
require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/Car.php';

class Search extends Action
{
   public function handle()
   {
      $query = Car::query()
         ->select("cars.*, brands.name as brand_name, types.name as type_name")
         ->join("brands", "brands.id = cars.brand_id")
         ->join("types", "types.id = cars.type_id");

      $brandId = $this->brand_id;

      $typeId = $this->type_id;

      if (!empty($brandId)) {

         $query->where("brand_id", "=", $brandId);
      }

      if (!empty($typeId)) {
         $query->where("type_id", "=", $typeId);
      }

      return $query->orderBy("cars.id", "desc")
         ->get();
   }
}
