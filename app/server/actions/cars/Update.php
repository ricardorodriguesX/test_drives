<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/Car.php';

class Update extends Action
{
   public function handle()
   {
      $car = Car::find($this->id);

      $this->abort_if(empty($car), 404);

      $car->update([
         "registration"  => $this->registration,
         "model"         => $this->model,
         "brand_id"      => $this->brand_id,
         "type_id"       => $this->type_id,
         "image"         => $this->image,
      ]);

      return $car->toArray();
   }
}
