<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/Customer.php';

class Create extends Action
{
   public function rules(&$validations = [])
   {
      $email = $this->email;

      $name = $this->name;

      $phone = $this->phone;

      $locality = $this->locality;

      if (empty($email)) {
         $validations["email"] = "Email não preenchido";
      }

      if (empty($name)) {
         $validations["name"] = "Nome não preenchido";
      }

      if (empty($phone)) {
         $validations["phone"] = "Telemóvel não preenchido";
      }

      if (empty($locality)) {
         $validations["locality"] = "Localidade não preenchido";
      }
   }

   public function handle()
   {
      $customer = Customer::query()->select("customers.*")->where("email", "=", $this->email)
         ->get();

      if (!empty($customer)) {
         return;
      }

      return Customer::create($this->attributes);
   }
}
