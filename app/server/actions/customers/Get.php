<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/Customer.php';

class Get extends Action
{
   public function handle()
   {
      $customer = Customer::query()->select("customers.*")->where("email", "=", $this->email)->get();

      $this->abort_if(empty($customer), 404);

      return $customer[0];
   }
}
