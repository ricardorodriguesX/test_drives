<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/TestDrive.php';

class Create extends Action
{
   public function handle()
   {
      return TestDrive::create([
         "customer_id"  => $this->customer_id,
         "car_id"         => $this->car_id,
         "date"          => $this->date,
      ]);
   }
}
