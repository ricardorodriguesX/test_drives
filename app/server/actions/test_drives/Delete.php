<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/TestDrive.php';

class Delete extends Action
{
   public function handle()
   {
      $testDrive = TestDrive::find($this->id);

      $this->abort_if(empty($testDrive), 404);

      $testDrive->delete();
   }
}
