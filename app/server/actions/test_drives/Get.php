<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/TestDrive.php';

class Get extends Action
{
   public function handle()
   {
      $testDrive = TestDrive::find($this->id);

      $this->abort_if(empty($testDrive), 404);

      return $testDrive;
   }
}
