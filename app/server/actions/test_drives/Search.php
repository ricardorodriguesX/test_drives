<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/TestDrive.php';

class Search extends Action
{
   public function handle()
   {
      $query = TestDrive::query()
         ->select("test_drives.*, cars.registration as car_registration, cars.model as car_model, brands.name as car_brand, types.name as car_type, customers.name as customer_name, customers.email as customer_email")
         ->join("cars", "cars.id = test_drives.car_id")
         ->join("brands", "brands.id = cars.brand_id")
         ->join("types", "types.id = cars.type_id")
         ->join("customers", "customers.id = test_drives.customer_id");

      $carId = $this->car_id;

      $customerId = $this->customer_id;

      if (!empty($carId)) {

         $query->where("car_id", "=", $carId);
      }

      if (!empty($customerId)) {

         $query->where("customer_id", "=", $customerId);
      }

      return $query
         ->orderBy("date", "desc")
         ->get();
   }
}
