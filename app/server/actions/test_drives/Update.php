<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/TestDrive.php';

class Update extends Action
{
   public function handle()
   {
      $test_drive = TestDrive::find($this->id);

      $this->abort_if(empty($test_drive), 404);

      $test_drive->update([
         "customer_id"  => $this->customer_id,
         "car_id"         => $this->car_id,
         "date"          => $this->date,
      ]);

      return $test_drive;
   }
}
