<?php

require_once __DIR__ . '/../Action.php';
require_once __DIR__ . '/../../models/Type.php';

class Search extends Action
{
   public function handle()
   {
      return Type::all();
   }
}
