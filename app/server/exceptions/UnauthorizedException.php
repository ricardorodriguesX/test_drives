<?php

require_once __DIR__ . '/RequestException.php';

class UnauthorizedException extends RequestException
{
}
