<?php

require_once __DIR__ . '/RequestException.php';

class ValidationException extends RequestException
{
}
