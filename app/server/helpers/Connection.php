<?php

class Connection
{
   private $host;

   private $database;

   private $user;

   private $password;

   private $connection;

   public function __construct()
   {
      $this->host = $this->env("DATABASE_HOST");

      $this->database = $this->env("DATABASE_NAME");

      $this->user = $this->env("DATABASE_USER");

      $this->password = $this->env("DATABASE_PASSWORD");

      $this->open();
   }

   public function __get($name)
   {
      return $this->{$name};
   }

   public function __set($name, $value)
   {
      return $this->{$name} = $value;
   }

   public function open()
   {
      if (!empty($this->connection)) {
         return $this->connection;
      }

      $this->connection = new PDO("mysql:host=$this->host;dbname=$this->database", $this->user, $this->password);
   }

   public function close()
   {
      if (empty($this->connection)) {
         return;
      }

      $this->connection = null;
   }

   private function env($key)
   {
      return parse_ini_file('.env')[$key];
   }
}
