<?php

require_once __DIR__ . '/../helpers/Connection.php';

abstract class Model implements JsonSerializable
{
   protected $table = __CLASS__;

   protected $id = "id";

   protected $connection;

   protected $query;

   protected $queryParameters = [];

   protected $attributes;

   public function __get($name)
   {
      return $this->attributes[$name];
   }

   public function __set($name, $value)
   {
      return $this->attributes[$name] = $value;
   }

   public function fill(array $data)
   {
      $this->attributes = $data;
   }

   public static function find($id)
   {
      $instance = new static;

      $connectionHelper = (new Connection());

      $instance->connection = $connectionHelper->connection;

      $instance->query = "SELECT * FROM $instance->table WHERE $instance->id = :id";

      $instance->queryParameters = [":id" => $id];

      $statment = $instance->connection->prepare($instance->query);

      $statment->execute($instance->queryParameters);

      $instance->attributes = $statment->fetch(PDO::FETCH_ASSOC);

      $connectionHelper->close();

      if (empty($instance->attributes)) {
         return null;
      }

      return $instance;
   }

   public static function query()
   {
      $instance = new static;

      $instance->query = "SELECT";

      return $instance;
   }

   public function select($fields = "*")
   {
      $this->query .= " $fields from $this->table";

      return $this;
   }

   public function join($table, $on)
   {
      $this->query .= " join $table on $on";

      return $this;
   }

   public static function create(array $data)
   {
      $instance = new static;

      $connectionHelper = (new Connection());

      $instance->connection = $connectionHelper->connection;

      $parameters = implode(',', array_keys($data));

      $parametersPrepared = [];

      array_walk($data, function ($item, $key) use (&$parametersPrepared) {
         $parametersPrepared[":$key"] = $item;
      });

      $values = implode(',', array_keys($parametersPrepared));

      $statment = $instance->connection->prepare("INSERT INTO $instance->table ($parameters) VALUES ($values)");

      $statment->execute($parametersPrepared);

      $data[$instance->id] = $instance->connection->lastInsertId();

      $connectionHelper->close();

      $instance->fill($data);

      return $instance;
   }

   public function update(array $data = [])
   {
      $this->attributes = array_merge($this->attributes, $data);

      $connectionHelper = (new Connection());

      $this->connection = $connectionHelper->connection;

      $parameters = implode(",", array_map(function ($value, $key) {
         return "$key = :$key";
      }, $this->attributes, array_keys($this->attributes)));

      $parametersPrepared = [];

      array_walk($this->attributes, function ($value, $key) use (&$parametersPrepared) {
         $parametersPrepared[":$key"] = $value;
      });

      $statment = $this->connection->prepare("UPDATE $this->table SET  $parameters WHERE $this->id = :$this->id");

      $statment->execute($parametersPrepared);

      $connectionHelper->close();

      return $this;
   }

   public function delete()
   {
      $connectionHelper = (new Connection());

      $this->connection = $connectionHelper->connection;

      $statment = $this->connection->prepare("DELETE FROM $this->table WHERE $this->id = :id");

      $statment->execute([
         ":id" => $this->{$this->id}
      ]);

      $connectionHelper->close();
   }

   public function where($column, $operator, $value)
   {
      $this->query .= empty($this->queryParameters) ? " WHERE " : " AND ";

      $this->query .= " ($column $operator :$column)";

      $this->queryParameters = array_merge($this->queryParameters, [":$column" => $value]);

      return $this;
   }

   public static function all()
   {
      $instance = new static;

      $connectionHelper = (new Connection());

      $instance->connection = $connectionHelper->connection;

      $instance->query = "SELECT * FROM $instance->table";

      $instance->queryParameters = [];

      $statment = $instance->connection->prepare($instance->query);

      $statment->execute();

      $rows = $statment->fetchAll(PDO::FETCH_ASSOC);

      $connectionHelper->close();

      $return = [];

      foreach ($rows as $row) {
         $instance = new static();

         $instance->fill($row);

         $return[] = $instance;
      }

      return $return;
   }

   public function orWhere($column, $operator, $value)
   {
      $this->query .= " or ($column $operator :$column)";

      $this->queryParameters = array_merge($this->queryParameters, [":$column" => $value]);

      return $this;
   }

   public function orderBy($field, $direction = "ASC")
   {
      $this->query .= " order by $field $direction";

      return $this;
   }

   public function get()
   {
      $connectionHelper = (new Connection());

      $this->connection = $connectionHelper->connection;

      $statment = $this->connection->prepare($this->query);

      $statment->execute($this->queryParameters);

      $rows = $statment->fetchAll(PDO::FETCH_ASSOC);

      $connectionHelper->close();

      $return = [];

      foreach ($rows as $row) {
         $instance = new static();

         $instance->fill($row);

         $return[] = $instance;
      }

      return $return;
   }

   public function toArray()
   {
      return $this->attributes;
   }

   public function jsonSerialize()
   {
      return $this->attributes;
   }
}
