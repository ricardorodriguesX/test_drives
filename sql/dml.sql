
INSERT INTO `brands` (`id`, `name`, `image`) VALUES
(1, 'Audi', '/app/public/assets/images/brands/audi.png'),
(2, 'BMW', '/app/public/assets/images/brands/bmw.png'),
(3, 'Mercedes-Benz', '/app/public/assets/images/brands/mercedes-benz.png'),
(4, 'Tesla', '/app/public/assets/images/brands/tesla.png'),
(5, 'Citroën', '/app/public/assets/images/brands/citroen.png');

INSERT INTO `types` (`id`, `name`) VALUES
(1, 'Carros elétricos'),
(2, 'Híbridos'),
(3, 'Híbridos plug-in'),
(4, 'Diesel'),
(5, 'Gasolina'),
(6, 'GLP');

INSERT INTO `cars` (`id`, `registration`, `model`, `brand_id`, `type_id`, `image`) VALUES
(1, 'AA-00-01', 'A6', 1, 2, '/app/public/assets/images/cars/AA-00-01.jpg'),
(2, 'AA-00-02', '520d', 2, 4, '/app/public/assets/images/cars/AA-00-02.jpg'),
(3, 'AA-00-03', 'Model X', 4, 1, '/app/public/assets/images/cars/AA-00-03.jpg'),
(4, 'AA-00-04', 'DS5', 5, 5, '/app/public/assets/images/cars/AA-00-04.jpg'),
(5, 'AA-00-05', 'Classe C', 3, 3, '/app/public/assets/images/cars/AA-00-05.jpg');